//
//  SurveyTableViewCell.swift
//  Survey
//
//  Created by Vipul Singhania on 26/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import UIKit

class SurveyTableViewCell: UITableViewCell {

    @IBOutlet weak var surveyImgView: UIImageView!
    @IBOutlet weak var surveyBtn: RoundButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(surveyModel:SurveyModel)  {
        self.surveyImgView.loadImage(url: surveyModel.coverImageUrl)
    }
    
}
