//
//	Question.swift
//  Survey
//
//  Created by Vipul Singhania on 25/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import Foundation


class Question : NSObject, NSCoding{

	var answers : [AnyObject]!
	var correctAnswerId : AnyObject!
	var coverBackgroundColor : AnyObject!
	var coverImageOpacity : Float!
	var coverImageUrl : String!
	var displayOrder : Int!
	var displayType : String!
	var facebookProfile : String!
	var fontFace : AnyObject!
	var fontSize : AnyObject!
	var helpText : AnyObject!
	var id : String!
	var imageUrl : AnyObject!
	var isMandatory : Bool!
	var isShareableOnFacebook : Bool!
	var isShareableOnTwitter : Bool!
	var pick : String!
	var shortText : String!
	var tagList : String!
	var text : String!
	var twitterProfile : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		answers = dictionary["answers"] as? [AnyObject]
		correctAnswerId = dictionary["correct_answer_id"] as? AnyObject
		coverBackgroundColor = dictionary["cover_background_color"] as? AnyObject
		coverImageOpacity = dictionary["cover_image_opacity"] as? Float
		coverImageUrl = dictionary["cover_image_url"] as? String
		displayOrder = dictionary["display_order"] as? Int
		displayType = dictionary["display_type"] as? String
		facebookProfile = dictionary["facebook_profile"] as? String
		fontFace = dictionary["font_face"] as? AnyObject
		fontSize = dictionary["font_size"] as? AnyObject
		helpText = dictionary["help_text"] as? AnyObject
		id = dictionary["id"] as? String
		imageUrl = dictionary["image_url"] as? AnyObject
		isMandatory = dictionary["is_mandatory"] as? Bool
		isShareableOnFacebook = dictionary["is_shareable_on_facebook"] as? Bool
		isShareableOnTwitter = dictionary["is_shareable_on_twitter"] as? Bool
		pick = dictionary["pick"] as? String
		shortText = dictionary["short_text"] as? String
		tagList = dictionary["tag_list"] as? String
		text = dictionary["text"] as? String
		twitterProfile = dictionary["twitter_profile"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if answers != nil{
			dictionary["answers"] = answers
		}
		if correctAnswerId != nil{
			dictionary["correct_answer_id"] = correctAnswerId
		}
		if coverBackgroundColor != nil{
			dictionary["cover_background_color"] = coverBackgroundColor
		}
		if coverImageOpacity != nil{
			dictionary["cover_image_opacity"] = coverImageOpacity
		}
		if coverImageUrl != nil{
			dictionary["cover_image_url"] = coverImageUrl
		}
		if displayOrder != nil{
			dictionary["display_order"] = displayOrder
		}
		if displayType != nil{
			dictionary["display_type"] = displayType
		}
		if facebookProfile != nil{
			dictionary["facebook_profile"] = facebookProfile
		}
		if fontFace != nil{
			dictionary["font_face"] = fontFace
		}
		if fontSize != nil{
			dictionary["font_size"] = fontSize
		}
		if helpText != nil{
			dictionary["help_text"] = helpText
		}
		if id != nil{
			dictionary["id"] = id
		}
		if imageUrl != nil{
			dictionary["image_url"] = imageUrl
		}
		if isMandatory != nil{
			dictionary["is_mandatory"] = isMandatory
		}
		if isShareableOnFacebook != nil{
			dictionary["is_shareable_on_facebook"] = isShareableOnFacebook
		}
		if isShareableOnTwitter != nil{
			dictionary["is_shareable_on_twitter"] = isShareableOnTwitter
		}
		if pick != nil{
			dictionary["pick"] = pick
		}
		if shortText != nil{
			dictionary["short_text"] = shortText
		}
		if tagList != nil{
			dictionary["tag_list"] = tagList
		}
		if text != nil{
			dictionary["text"] = text
		}
		if twitterProfile != nil{
			dictionary["twitter_profile"] = twitterProfile
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         answers = aDecoder.decodeObject(forKey: "answers") as? [AnyObject]
         correctAnswerId = aDecoder.decodeObject(forKey: "correct_answer_id") as? AnyObject
         coverBackgroundColor = aDecoder.decodeObject(forKey: "cover_background_color") as? AnyObject
         coverImageOpacity = aDecoder.decodeObject(forKey: "cover_image_opacity") as? Float
         coverImageUrl = aDecoder.decodeObject(forKey: "cover_image_url") as? String
         displayOrder = aDecoder.decodeObject(forKey: "display_order") as? Int
         displayType = aDecoder.decodeObject(forKey: "display_type") as? String
         facebookProfile = aDecoder.decodeObject(forKey: "facebook_profile") as? String
         fontFace = aDecoder.decodeObject(forKey: "font_face") as? AnyObject
         fontSize = aDecoder.decodeObject(forKey: "font_size") as? AnyObject
         helpText = aDecoder.decodeObject(forKey: "help_text") as? AnyObject
         id = aDecoder.decodeObject(forKey: "id") as? String
         imageUrl = aDecoder.decodeObject(forKey: "image_url") as? AnyObject
         isMandatory = aDecoder.decodeObject(forKey: "is_mandatory") as? Bool
         isShareableOnFacebook = aDecoder.decodeObject(forKey: "is_shareable_on_facebook") as? Bool
         isShareableOnTwitter = aDecoder.decodeObject(forKey: "is_shareable_on_twitter") as? Bool
         pick = aDecoder.decodeObject(forKey: "pick") as? String
         shortText = aDecoder.decodeObject(forKey: "short_text") as? String
         tagList = aDecoder.decodeObject(forKey: "tag_list") as? String
         text = aDecoder.decodeObject(forKey: "text") as? String
         twitterProfile = aDecoder.decodeObject(forKey: "twitter_profile") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if answers != nil{
			aCoder.encode(answers, forKey: "answers")
		}
		if correctAnswerId != nil{
			aCoder.encode(correctAnswerId, forKey: "correct_answer_id")
		}
		if coverBackgroundColor != nil{
			aCoder.encode(coverBackgroundColor, forKey: "cover_background_color")
		}
		if coverImageOpacity != nil{
			aCoder.encode(coverImageOpacity, forKey: "cover_image_opacity")
		}
		if coverImageUrl != nil{
			aCoder.encode(coverImageUrl, forKey: "cover_image_url")
		}
		if displayOrder != nil{
			aCoder.encode(displayOrder, forKey: "display_order")
		}
		if displayType != nil{
			aCoder.encode(displayType, forKey: "display_type")
		}
		if facebookProfile != nil{
			aCoder.encode(facebookProfile, forKey: "facebook_profile")
		}
		if fontFace != nil{
			aCoder.encode(fontFace, forKey: "font_face")
		}
		if fontSize != nil{
			aCoder.encode(fontSize, forKey: "font_size")
		}
		if helpText != nil{
			aCoder.encode(helpText, forKey: "help_text")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if imageUrl != nil{
			aCoder.encode(imageUrl, forKey: "image_url")
		}
		if isMandatory != nil{
			aCoder.encode(isMandatory, forKey: "is_mandatory")
		}
		if isShareableOnFacebook != nil{
			aCoder.encode(isShareableOnFacebook, forKey: "is_shareable_on_facebook")
		}
		if isShareableOnTwitter != nil{
			aCoder.encode(isShareableOnTwitter, forKey: "is_shareable_on_twitter")
		}
		if pick != nil{
			aCoder.encode(pick, forKey: "pick")
		}
		if shortText != nil{
			aCoder.encode(shortText, forKey: "short_text")
		}
		if tagList != nil{
			aCoder.encode(tagList, forKey: "tag_list")
		}
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}
		if twitterProfile != nil{
			aCoder.encode(twitterProfile, forKey: "twitter_profile")
		}

	}

}
