//
//	SurveyModel.swift
//  Survey
//
//  Created by Vipul Singhania on 25/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import Foundation


class SurveyModel : NSObject, NSCoding{

	var accessCodePrompt : AnyObject!
	var accessCodeValidation : String!
	var activeAt : String!
	var coverBackgroundColor : AnyObject!
	var coverImageUrl : String!
	var createdAt : String!
	var defaultLanguage : String!
	var descriptionField : String!
	var footerContent : String!
	var id : String!
	var inactiveAt : AnyObject!
	var isAccessCodeRequired : Bool!
	var isAccessCodeValidRequired : Bool!
	var isActive : Bool!
	var languageList : [String]!
	var questions : [Question]!
	var shortUrl : String!
	var surveyVersion : Int!
	var tagList : String!
	var thankEmailAboveThreshold : String!
	var thankEmailBelowThreshold : String!
	var theme : Theme!
	var title : String!
	var type : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		accessCodePrompt = dictionary["access_code_prompt"] as? AnyObject
		accessCodeValidation = dictionary["access_code_validation"] as? String
		activeAt = dictionary["active_at"] as? String
		coverBackgroundColor = dictionary["cover_background_color"] as? AnyObject
		coverImageUrl = dictionary["cover_image_url"] as? String
		createdAt = dictionary["created_at"] as? String
		defaultLanguage = dictionary["default_language"] as? String
		descriptionField = dictionary["description"] as? String
		footerContent = dictionary["footer_content"] as? String
		id = dictionary["id"] as? String
		inactiveAt = dictionary["inactive_at"] as? AnyObject
		isAccessCodeRequired = dictionary["is_access_code_required"] as? Bool
		isAccessCodeValidRequired = dictionary["is_access_code_valid_required"] as? Bool
		isActive = dictionary["is_active"] as? Bool
		languageList = dictionary["language_list"] as? [String]
		questions = [Question]()
		if let questionsArray = dictionary["questions"] as? [[String:Any]]{
			for dic in questionsArray{
				let value = Question(fromDictionary: dic)
				questions.append(value)
			}
		}
		shortUrl = dictionary["short_url"] as? String
		surveyVersion = dictionary["survey_version"] as? Int
		tagList = dictionary["tag_list"] as? String
		thankEmailAboveThreshold = dictionary["thank_email_above_threshold"] as? String
		thankEmailBelowThreshold = dictionary["thank_email_below_threshold"] as? String
		if let themeData = dictionary["theme"] as? [String:Any]{
			theme = Theme(fromDictionary: themeData)
		}
		title = dictionary["title"] as? String
		type = dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessCodePrompt != nil{
			dictionary["access_code_prompt"] = accessCodePrompt
		}
		if accessCodeValidation != nil{
			dictionary["access_code_validation"] = accessCodeValidation
		}
		if activeAt != nil{
			dictionary["active_at"] = activeAt
		}
		if coverBackgroundColor != nil{
			dictionary["cover_background_color"] = coverBackgroundColor
		}
		if coverImageUrl != nil{
			dictionary["cover_image_url"] = coverImageUrl
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if defaultLanguage != nil{
			dictionary["default_language"] = defaultLanguage
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if footerContent != nil{
			dictionary["footer_content"] = footerContent
		}
		if id != nil{
			dictionary["id"] = id
		}
		if inactiveAt != nil{
			dictionary["inactive_at"] = inactiveAt
		}
		if isAccessCodeRequired != nil{
			dictionary["is_access_code_required"] = isAccessCodeRequired
		}
		if isAccessCodeValidRequired != nil{
			dictionary["is_access_code_valid_required"] = isAccessCodeValidRequired
		}
		if isActive != nil{
			dictionary["is_active"] = isActive
		}
		if languageList != nil{
			dictionary["language_list"] = languageList
		}
		if questions != nil{
			var dictionaryElements = [[String:Any]]()
			for questionsElement in questions {
				dictionaryElements.append(questionsElement.toDictionary())
			}
			dictionary["questions"] = dictionaryElements
		}
		if shortUrl != nil{
			dictionary["short_url"] = shortUrl
		}
		if surveyVersion != nil{
			dictionary["survey_version"] = surveyVersion
		}
		if tagList != nil{
			dictionary["tag_list"] = tagList
		}
		if thankEmailAboveThreshold != nil{
			dictionary["thank_email_above_threshold"] = thankEmailAboveThreshold
		}
		if thankEmailBelowThreshold != nil{
			dictionary["thank_email_below_threshold"] = thankEmailBelowThreshold
		}
		if theme != nil{
			dictionary["theme"] = theme.toDictionary()
		}
		if title != nil{
			dictionary["title"] = title
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accessCodePrompt = aDecoder.decodeObject(forKey: "access_code_prompt") as? AnyObject
         accessCodeValidation = aDecoder.decodeObject(forKey: "access_code_validation") as? String
         activeAt = aDecoder.decodeObject(forKey: "active_at") as? String
         coverBackgroundColor = aDecoder.decodeObject(forKey: "cover_background_color") as? AnyObject
         coverImageUrl = aDecoder.decodeObject(forKey: "cover_image_url") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         defaultLanguage = aDecoder.decodeObject(forKey: "default_language") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         footerContent = aDecoder.decodeObject(forKey: "footer_content") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         inactiveAt = aDecoder.decodeObject(forKey: "inactive_at") as? AnyObject
         isAccessCodeRequired = aDecoder.decodeObject(forKey: "is_access_code_required") as? Bool
         isAccessCodeValidRequired = aDecoder.decodeObject(forKey: "is_access_code_valid_required") as? Bool
         isActive = aDecoder.decodeObject(forKey: "is_active") as? Bool
         languageList = aDecoder.decodeObject(forKey: "language_list") as? [String]
         questions = aDecoder.decodeObject(forKey :"questions") as? [Question]
         shortUrl = aDecoder.decodeObject(forKey: "short_url") as? String
         surveyVersion = aDecoder.decodeObject(forKey: "survey_version") as? Int
         tagList = aDecoder.decodeObject(forKey: "tag_list") as? String
         thankEmailAboveThreshold = aDecoder.decodeObject(forKey: "thank_email_above_threshold") as? String
         thankEmailBelowThreshold = aDecoder.decodeObject(forKey: "thank_email_below_threshold") as? String
         theme = aDecoder.decodeObject(forKey: "theme") as? Theme
         title = aDecoder.decodeObject(forKey: "title") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accessCodePrompt != nil{
			aCoder.encode(accessCodePrompt, forKey: "access_code_prompt")
		}
		if accessCodeValidation != nil{
			aCoder.encode(accessCodeValidation, forKey: "access_code_validation")
		}
		if activeAt != nil{
			aCoder.encode(activeAt, forKey: "active_at")
		}
		if coverBackgroundColor != nil{
			aCoder.encode(coverBackgroundColor, forKey: "cover_background_color")
		}
		if coverImageUrl != nil{
			aCoder.encode(coverImageUrl, forKey: "cover_image_url")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if defaultLanguage != nil{
			aCoder.encode(defaultLanguage, forKey: "default_language")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if footerContent != nil{
			aCoder.encode(footerContent, forKey: "footer_content")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if inactiveAt != nil{
			aCoder.encode(inactiveAt, forKey: "inactive_at")
		}
		if isAccessCodeRequired != nil{
			aCoder.encode(isAccessCodeRequired, forKey: "is_access_code_required")
		}
		if isAccessCodeValidRequired != nil{
			aCoder.encode(isAccessCodeValidRequired, forKey: "is_access_code_valid_required")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "is_active")
		}
		if languageList != nil{
			aCoder.encode(languageList, forKey: "language_list")
		}
		if questions != nil{
			aCoder.encode(questions, forKey: "questions")
		}
		if shortUrl != nil{
			aCoder.encode(shortUrl, forKey: "short_url")
		}
		if surveyVersion != nil{
			aCoder.encode(surveyVersion, forKey: "survey_version")
		}
		if tagList != nil{
			aCoder.encode(tagList, forKey: "tag_list")
		}
		if thankEmailAboveThreshold != nil{
			aCoder.encode(thankEmailAboveThreshold, forKey: "thank_email_above_threshold")
		}
		if thankEmailBelowThreshold != nil{
			aCoder.encode(thankEmailBelowThreshold, forKey: "thank_email_below_threshold")
		}
		if theme != nil{
			aCoder.encode(theme, forKey: "theme")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
