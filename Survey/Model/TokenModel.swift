//
//	TokenModel.swift
//  Survey
//
//  Created by Vipul Singhania on 23/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import Foundation

struct tokenResponseKey {
    static let kAccessToken = "access_token"
    static let kTokenType = "token_type"
    static let kCreated = "created_at"
    static let kExpiry = "expires_in"
}


class TokenModel : NSObject, NSCoding{

	var accessToken : String!
    var tokenType : String!
	var createdAt : Int!
	var expiresIn : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		accessToken = dictionary[tokenResponseKey.kAccessToken] as? String
        tokenType = dictionary[tokenResponseKey.kTokenType] as? String
		createdAt = dictionary[tokenResponseKey.kCreated] as? Int
		expiresIn = dictionary[tokenResponseKey.kExpiry] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessToken != nil{
			dictionary[tokenResponseKey.kAccessToken] = accessToken
		}
        if tokenType != nil{
            dictionary[tokenResponseKey.kTokenType] = tokenType
        }
		if createdAt != nil{
			dictionary[tokenResponseKey.kCreated] = createdAt
		}
		if expiresIn != nil{
			dictionary[tokenResponseKey.kExpiry] = expiresIn
		}
		return dictionary
	}
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        accessToken = aDecoder.decodeObject(forKey: tokenResponseKey.kAccessToken) as? String
        createdAt = aDecoder.decodeObject(forKey: tokenResponseKey.kCreated) as? Int
        expiresIn = aDecoder.decodeObject(forKey: tokenResponseKey.kExpiry) as? Int
        tokenType = aDecoder.decodeObject(forKey: tokenResponseKey.kTokenType) as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if accessToken != nil{
            aCoder.encode(accessToken, forKey: tokenResponseKey.kAccessToken)
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: tokenResponseKey.kCreated)
        }
        if expiresIn != nil{
            aCoder.encode(expiresIn, forKey: tokenResponseKey.kExpiry)
        }
        if tokenType != nil{
            aCoder.encode(tokenType, forKey: tokenResponseKey.kTokenType)
        }
        
    }



}
