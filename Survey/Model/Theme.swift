//
//	Theme.swift
//  Survey
//
//  Created by Vipul Singhania on 25/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//
import Foundation


class Theme : NSObject, NSCoding{

	var colorActive : String!
	var colorAnswerInactive : String!
	var colorAnswerNormal : String!
	var colorInactive : String!
	var colorQuestion : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		colorActive = dictionary["color_active"] as? String
		colorAnswerInactive = dictionary["color_answer_inactive"] as? String
		colorAnswerNormal = dictionary["color_answer_normal"] as? String
		colorInactive = dictionary["color_inactive"] as? String
		colorQuestion = dictionary["color_question"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if colorActive != nil{
			dictionary["color_active"] = colorActive
		}
		if colorAnswerInactive != nil{
			dictionary["color_answer_inactive"] = colorAnswerInactive
		}
		if colorAnswerNormal != nil{
			dictionary["color_answer_normal"] = colorAnswerNormal
		}
		if colorInactive != nil{
			dictionary["color_inactive"] = colorInactive
		}
		if colorQuestion != nil{
			dictionary["color_question"] = colorQuestion
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         colorActive = aDecoder.decodeObject(forKey: "color_active") as? String
         colorAnswerInactive = aDecoder.decodeObject(forKey: "color_answer_inactive") as? String
         colorAnswerNormal = aDecoder.decodeObject(forKey: "color_answer_normal") as? String
         colorInactive = aDecoder.decodeObject(forKey: "color_inactive") as? String
         colorQuestion = aDecoder.decodeObject(forKey: "color_question") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if colorActive != nil{
			aCoder.encode(colorActive, forKey: "color_active")
		}
		if colorAnswerInactive != nil{
			aCoder.encode(colorAnswerInactive, forKey: "color_answer_inactive")
		}
		if colorAnswerNormal != nil{
			aCoder.encode(colorAnswerNormal, forKey: "color_answer_normal")
		}
		if colorInactive != nil{
			aCoder.encode(colorInactive, forKey: "color_inactive")
		}
		if colorQuestion != nil{
			aCoder.encode(colorQuestion, forKey: "color_question")
		}

	}

}
