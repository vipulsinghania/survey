//
//  UIViewControllerExtension.swift
//  Survey
//
//  Created by Vipul Singhania on 24/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

extension UIViewController {

    
    func showAlert(_ msg : String, handler:(()->Void)? = nil) {
        
        if let topController = UIApplication.topViewController() {
            
            let alert = UIAlertController(title: "", message: msg as String, preferredStyle: .alert)
            
            let actionOK = UIAlertAction(title: "OK", style: .default, handler: { (action : UIAlertAction) in
                handler?()
            })
            alert.addAction(actionOK)
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlert(_ title:String, msg:String) {
        
        if let topController = UIApplication.topViewController() {
            let alert = UIAlertController(title: title as String , message: msg as String, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
            topController.present(alert, animated: true, completion: nil)
        }
    }
    


}
