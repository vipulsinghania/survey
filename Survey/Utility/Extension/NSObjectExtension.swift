//
//  NSObjectExtension.swift
//  Survey
//
//  Created by Vipul Singhania on 24/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

extension NSObject{
    
    func getdataFromKeychain(key:String) -> TokenModel? {
        if let data =  KeychainWrapper.standard.object(forKey: key){
            return data as? TokenModel
        }
        return nil
    }

    func saveDataToKeychain(tokenModel:TokenModel){
        
        KeychainWrapper.standard.set(tokenModel, forKey: "token", withAccessibility: .whenUnlocked)
    }

    

}
