//
//  StringExtension.swift
//  Survey
//
//  Created by Vipul Singhania on 24/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIImageView  {
    var photosManager: PhotosManager { return .shared }

    func loadImage(url:String) {
        if let image = photosManager.cachedImage(for: url) {
            populate(with: image)
            return
        }
        downloadImage(url:url)
    }
    
    private func downloadImage(url:String) {
       _ = photosManager.retrieveImage(for: url) { image in
            self.populate(with: image)
        }
    }
    
    private func populate(with image: UIImage) {
        self.image = image
    }

}
