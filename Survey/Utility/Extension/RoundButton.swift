//
//  File.swift
//  Survey
//
//  Created by Vipul Singhania on 25/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//
//

import UIKit

@IBDesignable open class RoundButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    override open func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
    }
}
