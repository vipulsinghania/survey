//
//  AppConstants.swift
//  Survey
//
//  Created by Vipul Singhania on 22/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

// MARK: - URL
import UIKit

let kBaseURL = "https://nimbl3-survey-api.herokuapp.com/"

//     MARK: - End point
let kSurveyEndPoint = "surveys.json"
let kAuthEndPoint = "oauth/token"



struct ScreenSize {
    static let kWidth:CGFloat = UIScreen.main.bounds.size.width
    static let kHeight:CGFloat = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.kWidth, ScreenSize.kHeight)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.kWidth, ScreenSize.kHeight)
    
}

//enum Screen: String {
//    case WelcomeVC, SignUpVC, SignInVC, ResetPasswordVC, SettingsOptionVC, WiFiVC, BleVC, AccountVC
//}

