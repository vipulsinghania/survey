//
//  ViewController.swift
//  Survey
//
//  Created by Vipul Singhania on 22/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

struct SurveyParam {
    static let kPage = "page"
    static let kPerPage = "per_page"
}



class ViewController: UIViewController {

    var pageCount = 1
    var surveyList = [SurveyModel]()
    let cellReuseIdentifier = String(describing: SurveyTableViewCell.self)

    @IBOutlet weak var surveyTblView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageControl.transform = pageControl.transform.rotated(by: .pi/2)
        pageControl.currentPage = 0
        surveyTblView.register(UINib (nibName:"SurveyTableViewCell", bundle:nil), forCellReuseIdentifier: cellReuseIdentifier)

        let tokenmodel = self.getdataFromKeychain(key: "token")
        if tokenmodel == nil {
            self.getOAuthToken(true)
        }else{
            self.getSurveyList(showloader: true)
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func getSurveyList(showloader:Bool)  {
        var paramDict = Dictionary<String,Any>()
        paramDict[SurveyParam.kPage] = pageCount
        paramDict[SurveyParam.kPerPage] = "20"
        
        ApiManager.sharedInstance.requestGET(requestMethod: kSurveyEndPoint, parameter:paramDict, showLoader: showloader, successBlock: { (response : [AnyObject], message : String?) in
            for item in response{
                let surveyModel = SurveyModel(fromDictionary: item as! [String : Any])
                self.surveyList.append(surveyModel)
            }
            self.pageControl.numberOfPages = self.surveyList.count

            self.surveyTblView.reloadData()
            
        }) { (response : Dictionary<String, Any>?, error : Error?, code: Int) in
            if code == 401{
                self.getOAuthToken(true)
            }
            if response != nil {
            }
        }
        
        
    }
    
    func getOAuthToken(_ showLoader: Bool)  {
        var paramDict = Dictionary<String,String>()
        paramDict[OAuthParam.kGrantType] = "password"
        paramDict[OAuthParam.kUsername] = "carlos@nimbl3.com"
        paramDict[OAuthParam.kPassword] = "antikera"
        
        ApiManager.sharedInstance.requestPOST(requestMethod: kAuthEndPoint, parameter:paramDict, showLoader: showLoader, successBlock: { (response : Dictionary<String, Any>, message : String?) in
            print(response)
            let tokenModel = TokenModel(fromDictionary: response)
            self.saveDataToKeychain(tokenModel: tokenModel)
            self.getSurveyList(showloader: true)

        }) { (response : Dictionary<String, Any>?, error : Error?) in
            
            if response != nil {
            }
        }
    }
    @IBAction func surveyBtnTapped(sender:UIButton) {
        let storyBoard = UIStoryboard(name:"Main", bundle:nil) // second storyboard name is given as second
        let controller = storyBoard.instantiateViewController(withIdentifier: "QuestionViewControllerID")
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    func refresh() {
        pageCount = 1
        self.getSurveyList(showloader: true)

    }

    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return surveyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)! as! SurveyTableViewCell
        cell.surveyBtn.addTarget(self, action:#selector(surveyBtnTapped(sender:)), for: .touchUpInside)

        cell.configureCell(surveyModel: surveyList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return ScreenSize.kHeight-64
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        pageControl.currentPage = indexPath.row
        if self.surveyList.count-5 == indexPath.row {
            pageCount += 1
            self.getSurveyList(showloader: false)
        }

    }
    
}


