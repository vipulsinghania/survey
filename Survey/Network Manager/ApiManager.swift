//
//  ApiManager.swift
//  Survey
//
//  Created by Vipul Singhania on 24/10/17.
//  Copyright © 2017 Vipul Singhania. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

struct OAuthParam {
    static let kGrantType = "grant_type"
    static let kUsername = "username"
    static let kPassword = "password"
}



class ApiManager: NSObject {
    
    var hud : MBProgressHUD!
    var request:DataRequest?
    static let sharedInstance = ApiManager()
    
    func requestGET(requestMethod : String, parameter : [String: Any], showLoader : Bool, successBlock:@escaping ((_ response : [AnyObject], _ message : String?)->Void), failureBlock:@escaping ((_ response : Dictionary<String,Any>?, _ error : Error?, _ code: Int) -> Void)) {

        let requestURL = kBaseURL.appending(requestMethod)
        if showLoader{
            self.addLoaderToView()
        }
        var httpheader = self.createHeader()
        let tokenmodel = self.getdataFromKeychain(key: "token")
        httpheader["Authorization"] = String(format:"%@ %@",(tokenmodel?.tokenType)! ,(tokenmodel?.accessToken)!)

        Alamofire.request(requestURL, method: .get, parameters:parameter, headers: httpheader).responseJSON  { response in
           
            if showLoader{
                self.removeLoaderFromView()
            }
            
            switch response.result{
                case .success:
                    if !(response.result.value is NSNull){
                        let jsonDict = response.result.value as! [AnyObject]
                        successBlock(jsonDict, nil)
                    }
                case .failure(let error):
                    failureBlock(nil, error, (response.response?.statusCode)!)
            }
        }
    }
    
    
    func requestPOST(requestMethod : String, parameter : [String: Any], showLoader : Bool, successBlock:@escaping ((_ response : Dictionary<String,Any>, _ message : String?)->Void), failureBlock:@escaping ((_ response : Dictionary<String,Any>?, _ error : Error?) -> Void)) {
        
        let requestURL = kBaseURL.appending(requestMethod)
        if showLoader{
            self.addLoaderToView()
        }
        
        Alamofire.request(requestURL, method: .post, parameters:parameter, headers: self.createHeader()).responseJSON  { response in
            
            if showLoader{
                self.removeLoaderFromView()
            }
            

            switch response.result {
                case .success:
                    let jsonDict = response.result.value as! Dictionary<String, Any>
                        successBlock(jsonDict, nil)
                case .failure(let error):
                    self.showError(errorDict: nil, error: error)
                    failureBlock(nil, error)
                    //                print(error)
            }
        }
    }

    
    
    func addLoaderToView() {
        let window = AppDelegate.sharedInstance.window?.rootViewController
        
        if window != nil {
            hud = MBProgressHUD .showAdded(to: window!.view, animated: true)
        } else {
            hud = MBProgressHUD()
        }
        hud.mode = .indeterminate
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.darkGray
        hud.contentColor = UIColor.white

    }
    
    func removeLoaderFromView() {
        hud.hide(animated: true)
    }
    
    func showError(errorDict:Dictionary<String,Any>?, error:Error?) {
        let msgStr = (error?.localizedDescription)!

        UIApplication.topViewController()?.showAlert("", msg:msgStr  )

    }

    
    func createHeader() -> HTTPHeaders  {
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return headers
    }
    
    func checkNetwork() -> Bool  {
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        net?.listener = {status in
            
            if  net?.isReachable ?? false {
                
                print("connection")

            }
            else {
                print("no connection")
            }
        
        }
        return (net?.isReachable)!
    }
    

    
}
